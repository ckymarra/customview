package com.example.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

public class CustomView extends LinearLayout implements View.OnClickListener {

    private int counter;

    private TextView tvName;
    private TextView tvCounter;

    private Button btnMinus;
    private Button btnPlus;
    private Button btn1pt;
    private Button btn2pt;
    private Button btn3pt;


    public CustomView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs){
        counter = 0;
        inflate(context, R.layout.layout, this);
        findViewsByIds();
        setButtonOptions();
        getAttrs(context, attrs);
    }

    private void findViewsByIds() {
        tvName = findViewById(R.id.tvName);
        tvCounter = findViewById(R.id.tvCounter);
        btnMinus = findViewById(R.id.btnMinus);
        btnPlus = findViewById(R.id.btnPlus);
        btn1pt = findViewById(R.id.btn1pt);
        btn2pt = findViewById(R.id.btn2pt);
        btn3pt = findViewById(R.id.btn3pt);
    }

    private void setButtonOptions() {
        btnMinus.setClickable(true);
        btnPlus.setClickable(true);
        btn1pt.setClickable(true);
        btn2pt.setClickable(true);
        btn3pt.setClickable(true);

        btnMinus.setOnClickListener(this);
        btnPlus.setOnClickListener(this);
        btn1pt.setOnClickListener(this);
        btn2pt.setOnClickListener(this);
        btn3pt.setOnClickListener(this);
    }

    private void getAttrs(Context context, AttributeSet attrs){
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomView);
        tvName.setText(a.getText(R.styleable.CustomView_text));
        a.recycle();
    }

    @Override
    public void onClick(View view) {
        if (view == btnMinus){
            tvCounter.setText(counter++);
        }else if(view == btnPlus){
            tvCounter.setText(counter--);
        }
    }
}
